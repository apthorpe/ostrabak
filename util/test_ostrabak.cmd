@rem Assume this script is run from the ostrabak bin directory
rem Convenience script to pretend to back up all Ostranauts savegames with extra diagnostics
cmd.exe /e:off /f:off /c ostrabak.exe --verbose --dry-run --all
pause