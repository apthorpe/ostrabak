@rem Assume this script is run from the ostrabak bin directory
rem Convenience script to back up all Ostranauts savegames, overwriting existing backups
cmd.exe /e:off /f:off /c ostrabak.exe --force --all
pause