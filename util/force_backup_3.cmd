@rem Assume this script is run from the ostrabak bin directory
rem Convenience script to back up Ostranauts savegame 3, overwriting existing backups
cmd.exe /e:off /f:off /c ostrabak.exe --force 3
pause