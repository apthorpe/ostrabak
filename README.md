# Ostrabak

## Introduction

Ostrabak is an experimental savegame backup and report collating utility for [Ostranauts](https://bluebottlegames.com/games/ostranauts), the *noir* spaceship-life sim from [Blue Bottle Games ](https://bluebottlegames.com/games/ostranauts)

Ostrabak has three main goals or functions:

1. Back up character save directories to a central folder (rename to show both save slot and the character name)
2. Create thin archives for issue reporting (skip `.bak` files/directories, include `output_log.txt` from game asset directory, datestamp directory)
3. Provide a straightforward CLI programming project to justify learning to program in Rust


## `tl;dr`

Double-click `force_backup_all.cmd` under `Program Files\ostrabak\bin`; backups are located in
`C:\Users\<username>\AppData\LocalLow\Blue Bottle Games\Ostranauts\_backups`

## Background and Operation

At present (Ostranauts 0.7.0.6) the game has just three save directories,
creatively named `Save1`, `Save2`, and `Save3`. These are typically located
in the directory `C:\Users\<username>\AppData\LocalLow\Blue Bottle Games\Ostranauts`.
Inside each save directory, the file `saveInfo.json` contains information
about the last player save such as character name and when the game was last saved.

Here's an example:
~~~json
[
  {
    "strName" : "3",
    "playerName" : "Rachel Bruce",
    "shipName"   : "K-Leg: Port Azikiwe",
    "saveNote"   : "",
    "formerOccupation" : "Shipbreaker",
    "version"          : "Early Access Build: 0.7.0.1\r\n",
    "age"              : 27.082999996841,
    "money"            : 419890.810139245,
    "playTimeElapsed"  : 25589.21,
    "simTimeElapsed"   : 47688.17578125,
    "simTimeCurrent"   : 65627546541.5186,
    "realWorldTime"    : "2021-11-22 19:17:21"
  }
]
~~~

`strName` corresponds to the save slot so this example was found in the directory
`C:\Users\<username>\AppData\LocalLow\Blue Bottle Games\Ostranauts\Save3`

### Backups

ostrabak extracts the character name (`Rachel Bruce`) and combines it with the
directory name (`Save3`) and copies the entire save directory to
`C:\Users\<username>\AppData\LocalLow\Blue Bottle Games\Ostranauts\_backups\Save3_Rachel Bruce`.
The `_backups` directory will be created if it doesn't already exist.

To see what ostrabak will try to do without actually creating directories and copying files, use:

~~~bat
$ ostrabak.exe --dry-run 3
[dry-run] Backing up Save3 (Rachel Bruce) to C:\Users\<username>\AppData\LocalLow\Blue Bottle Games\Ostranauts\_backups\Save3_Rachel Bruce
~~~

To restore this backup, copy the contents back to
`C:\Users\<username>\AppData\LocalLow\Blue Bottle Games\Ostranauts\Save3`.
Restoration is a manual process, ostrabak only reads live savegames and only writes to the `_backups` and `_reports` directories.

To back up all characters which have not previously been backed up, use:

~~~bat
$ ostrabak.exe --all
~~~

By default, ostrabak will not overwrite old backups but you can force it
to overwrite with the `--force` option:

~~~bat
ostrabak.exe --force --all
~~~

For simplicity, ostrabak is written as a command-line tool but you can wrap
it in a `.cmd` script so it can run just by double-clicking or hot-keying.
The ostrabak installer creates a few convenience scripts to make backups simple.

For example, running `force_backup_all.cmd` will back up all current savegames
and overwrite any previous backups:

~~~bat
@rem Assume this script is run from the ostrabak bin directory
rem Convenience script to back up all Ostranauts savegames, overwriting existing backups
cmd.exe /e:off /f:off /c ostrabak.exe --force --all
pause
~~~

This will pop up a Windows command shell and show you the progress of the
backups then keep the shell open until you hit a key. This is done so you can
see if there are any errors.

`test_ostrabak.cmd` does a safe dry-run backup of all savegames (nothing is written) and shows extra diagnostics:

~~~bat
@rem Assume this script is run from the ostrabak bin directory
rem Convenience script to pretend to back up all Ostranauts savegames with extra diagnostics
cmd.exe /e:off /f:off /c ostrabak.exe --verbose --dry-run --all
pause
~~~

`force_backup_1.cmd` does a force-backup on the first savegame:

~~~bat
@rem Assume this script is run from the ostrabak bin directory
rem Convenience script to back up Ostranauts savegame 1, overwriting existing backups
cmd.exe /e:off /f:off /c ostrabak.exe --force 1
pause
~~~

`force_backup_2.cmd` and `force_backup_3.cmd` are identical but
back up savegames 2 and 3 respectively.

Take these `cmd` scripts as examples. Remove `pause` and change
`/c` to `/k` if you want the command shell to close on completion.
Also the `/e:off` and `/f:off` options can be safely removed.

Note that `ostrabak.exe` itself doesn't care what directory it's
launched from. It finds your home directory (`C:\Users\<username>` and
appends `\AppData\LocalLow\Blue Bottle Games\Ostranauts\`
to get the main save directory then verifies that directory exists.

### Reports

The report generation procesds is similar to backup with a few minor execptions. First, ostrabak only copies the most recent main save file or directory and ignores all the `.bak` entries. This significantly reduces the amount of space a report consumes and makes it easier to compress to a size that Discord will accept in a post.

Second, `--report` copies `C:\Program Files (x86)\Steam\SteamApps\common\Ostranauts\Ostranauts_Data\output_log.txt` to the report directory along with the savedame info.

Finally, reports are datestamped and stored under `C:\Users\<username>\AppData\LocalLow\Blue Bottle Games\Ostranauts\_reports`. Extending the earlier example, the player name, save directory, and the time of the last save (`realWorldTime` from `saveInfo.json`) are combined into the report directory name `C:\Users\<username>\AppData\LocalLow\Blue Bottle Games\Ostranauts\_reports\Save3_Rachel Bruce_20211122191721`

Again, the `_reports` directory will be created if it doesn't already exist.

At this point the user should zip up `Save3_Rachel Bruce_20211122191721` into an archive (`.zip`, `.7z`, *etc.*) and send it to Blue Bottle Games by whatever means they prefer. Posting to the `#bugs` channel of the Ostranauts Discord server is effective but that may change in the future; I'm not part of the development team so please check with them for official advice.

## Usage

```bat
ostrabak v0.1.1.2: Savegame archiver for Ostranauts
Usage: Debug\ostrabak.exe [options] SAVEID [ SAVEID ... ]

Options:
    -a, --all           Back up all save files
    -F, --force         Overwrite existing character backup
    -h, --help          Print this help menu
    -l, --list          List current saved games
    -n, --dry-run       Find but do not copy files
    -r, --report        Collate for reporting an issue
    -v, --verbose       Show more detailed log info
    -V, --version       Print version info and exit
```

`SAVEID` is `1`, `2`, or `3` corresponding to the first, second, and third savegames respectively. To back up all savegames, use `--all`. To list the names and save times of the current savegames, use `--list`.

## Known issues

None known at present. `--all` does not work with `--report` but that's intentional.

## Design Criteria

Ostrabak is intended to be very simple and functional, putting very few demands on the user. It assumes the user just wants to play Ostranauts and occasionally back up their games or compile info for a bug report, not be a sysadmin or programmer.

It should requires only the installation of the Ostrabak utility and the Ostranauts game. **It specifically does not require the user to install development tools, WSL, MINGW/MSYS2, and so on**. No Python, no fake UNIX.

* It can be run by an unprivileged user who has full (*read-write-execute*) permissions on their Ostranauts save directory and read privileges on the Ostranauts asset directories (for reading/copying `output_log.txt`)
* It should run with default user permissions and does not require elevated privileges, Administrator access, or adjustment to the default Windows security model.
* It should run locally with no need of external resources, networking, web access, *etc.*
* It can be installed and uninstalled easily and obviously via a standard Windows installer package (for example, `setup.exe`, `*.msi`, `*.nupkg`). No manual copying, no app store, no restart after installation/removal.

For more information on design, see `DESIGN.md`

## Project Contributions

Contributions, bug reports, and feature requests are welcome! See [Ostrabak's gitlab issue tracker](https://gitlab.com/apthorpe/ostrabak/-/issues) for details. Please review `CODE_OF_CONDUCT.md`. `tl;dr` treat others with respect and dignity.

## Copyright

With the exception of files described under **Image Credits**, the Ostrabak project is copyright 2021, Bob Apthorpe.

### License

The Ostrabak souce code and documentation are released under an MIT license located in the file `LICENSE`. See **Image Credits** for more information.

### Image Credits

The project icon is `ItmFridge01Loose.png` which was taken from the Ostranaut game assets, copyright 2020, Blue Bottle Games
