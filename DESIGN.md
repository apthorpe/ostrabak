# Ostrabak Design Notes

## Introduction and Goals

`ostrabak` is an experimental Ostranauts savegame backup and report collating utility which has three main goals:

1. Back up character save directories to a central folder (rename to show both save slot and the character name)
2. Create thin archives for issue reporting (skip `.bak` files, include `output_log.txt` from game asset directory, datestamp directory)
3. Provide a straightforward CLI programming project to justify learning to program in Rust

## Assumptions

* Run by an unprivileged user who has full (`rwx`) permissions on their Ostranauts save directory and read privileges on the Ostranauts asset directories (for reading/copying `output_log.txt`)
* Runs with default user permissions; does not require elevated privileges, Administrator access, or adjustment to the default Windows security model
* Requires only the installation of the Ostrabak utility and the Ostranauts game. **Specifically does not require the user to install development tools, WSL, MINGW/MSYS2, and so on**. No Python, no fake unix.
* Runs locally with no need of external resources, networking, web access, *etc.*
* Can be installed and uninstalled easily and obviously

## Choosing an Implementation Language

Ostrabak should be easily implementable as a DOS command shell or PowerShell script but for various (IMO stupid and unfixable) reasons, it can't be.

### Evaluating DOS Command Shell

The traditional DOS command shell does not have usable text parsing and manipulation tools such as `sed`, making it overly-complicated and maybe impossible to reliably isolate the character name for renaming the save directories. It may be possible to use `FOR /f "tokens=... delims=..." %%K IN ( ... ) DO ( ... )` to parse out the character name from `saveInfo.json` but it is too complex and brittle to be trusted.

### Evaluating PowerShell

PowerShell is the next most obvious solution and while it has the capabilities needed, the default Windows security model prevents end users from running PowerShell scripts, even those they create themselves and which do not perform any privileged actions. It is unreasonable to demand the end user adjust Windows security settings just to run a script that automates tasks they could perform just by typing the commands into a PowerShell console. PowerShell is promoted as the correct tool to use for this sort of utility but the Windows security model prevents it from being a viable solution.

### Evaluating Windows System for Linux (WSL)

Ostrabak could easily be implemented using a traditional UNIX toolset consisting of a Bourne-compatible shell, `sed`, `grep`, `mkdir`, `cp`, `rm`, `rmdir`, and `date`. Implementing Ostrabak as a `bash` script under WSL would be straightforward but WSL is not installed by default and it is unreasonable to demand end users install a virtual Linux environment just to run a simple script to move a few files around.

### Evaluating Interpreted and Scripting Languages

The same argument can be made for installing any development tool, environment, or scripting language, be it AutoHotKey, Python, Lua, C#, *etc.* - it is unreasonable to demand end users install developer tooling or extra utilities just to run a simple script to move a few files around.

### Evaluating Compiled and Bytecode Languages

To minimize end user effort, administrative overhead, resource requirements, and the risk of misconfiguring security settings, it was decided to implement Ostrabak as a single standalone executable compiled from source code.

Reasonable language choices are C, C++, Rust, *etc.* C is difficult (for me) to program safely and is too low-level for this task. C++ has most of the disadvantages of C but adds unreasonable cognitive overhead from being far too abstract. Simply put, C++ offers so much flexibility and choice of abstraction that one spends most of their time trying to find the appropriate abstractions and make them concrete instead of actually solving the problem the code is intended to solve. Experienced C++ programmers may not have this problem but I don't consider myself an experienced C++ programmer.

What is desired is a language that is higher level than C but with less distracting flexibility than C++ with a reasonable standard library that can compile statically-linked executables (no dependencies on the CLR, JVM, *etc.*). Rust is a convenient cross-platform choice. Even though this particular project only needs to run on Windows (it's tied to whatever platform(s) the Ostranauts game natively runs on), Rust is not tied to a particular platform or vendor so skills gained in this project are generally applicable and transferrable.

## PDL Implementation Plan

The basic process to be implemented is described below in [Program Design Language](https://en.wikipedia.org/wiki/Program_Design_Language), a form of pseudocode that errs toward human language *vs* a generalized pomputer language.

### Initial Backup Feature

~~~
Get command line arguments for processing into program location, options, and positional parameters
Detect home directory
Define complete [valid saveid list]: `['1', '2', '3']`
Detect path to Ostranauts saveganes
Create `savedir/_backups` if it doesn't already exist
If --all:
  Set positional arguments list to complete [valid saveid list]
Else:
  Normalize positional arguments to unique list
  Q: Warn and drop any unique positional argument not in [valid saveid list]?
Endif

(If not --report):
Foreach unique positional argument (saveid):
  Warn and cycle if saveid not in [valid saveid list] Q: Or do earlier?
  Construct savegame path (savedir/Save# where # is saveid)
  Warn and cycle if savegame path is not found (do not warn if --all is set)
  Warn and cycle if savedir/Save#/saveInfo.json not found
  Warn and cycle if playerName value cannot be extracted from savedir/Save#/saveInfo.json
  Extract playerName value as string (Q: mutable?)
  Construct backup path of current savegame -> "savedir/_backups/Save# <playerName>"
    Q: Replace spaces in backup path with underscores?
  If backup path does not exist:
    Recursively copy "savedir/Save#"" to "savedir/_backups/Save# <playerName>"
  Else:
    If --force is set:
      Delete existing "savedir/_backups/Save# <playerName>"
      Recursively copy "savedir/Save#"" to "savedir/_backups/Save# <playerName>"
    Else:
      Warn of backup collision and cycle
    Endif
  Endif
End foreach

Print summary of directories created (path)
~~~

### Extended Report Collation Feature

**TBD1** indicates an extension to the basic backup functionality to simplify gathering and submitting user files when reporting issues.

~~~
TBD1: If --report:
Foreach unique positional argument (saveid):
  Note: Steps marked with * are identical to those in the backup process above
  * Warn and cycle if saveid not in [valid saveid list] Q: Or do earlier?
  * Construct savegame path (savedir/Save# where # is saveid)
  * Warn and cycle if savegame path is not found (do not warn if --all is set)
  * Warn and cycle if savedir/Save#/saveInfo.json not found
  * Warn and cycle if playerName value cannot be extracted from savedir/Save#/saveInfo.json
  * Extract playerName value as string (Q: mutable?)
  Construct report path of current savegame -> "savedir/_reports/YYYMMMDDHHMMSS Save# <playerName>"
    Q: Replace spaces in backup path with underscores?
  While report path exists:
    Warn and sleep(2)
    Warn and cycle foreach if 5 attempts fail (failsafe)
    Regenerate report path with new timestamp
  End while
    //
  Foreach file in [saveInfo.json, <playerName>.json, portrait.png]:
    Copy from "savedir/Save#" to "savedir/_reports/YYYMMMDDHHMMSS Save# <playerName>"
  End foreach
  Copy C:\Program Files (x86)\Steam\SteamApps\common\Ostranauts\Ostranauts_Data/output_log.txt to "savedir/_reports/YYYMMMDDHHMMSS Save# <playerName>" or warn
  Archive and compress "savedir/_reports/YYYMMMDDHHMMSS Save# <playerName>" to "savedir/_reports/YYYMMMDDHHMMSS Save# <playerName>.zip"
  Q: Can files be archived directly to zipfile without creating an uncompressed directory?
End foreach

~~~
