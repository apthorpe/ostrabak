# Image Credits

The file `ItmFridge01Loose.png` is taken from the Ostranaut game assets, copyright 2020, Blue Bottle Games. `Fridge.ico` is `ItmFridge01Loose.png` saved in `.ico` format with [XnView](https://www.xnview.com/en/)