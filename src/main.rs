// Experimental Ostranauts savegame backup and report collating utility
extern crate dirs;
// extern crate env_logger;
// extern crate fs_extra;
extern crate getopts;
extern crate json;
extern crate lazy_static;
// extern crate simple_logger;

// use fs_extra::dir::copy;
use getopts::Options;
use lazy_static::lazy_static;
// use log::{debug, error, log_enabled, info, Level, LevelFilter};
use log::{debug, info, warn, error};
// use serde::{Deserialize, Serialize};
// use serde_json::{Result, Value};
use regex::Regex;
use simple_logger::SimpleLogger;
use std::collections::HashSet;
use std::env;
// use std::fmt::format;
use std::io;
use std::fs;
use std::iter::FromIterator;
use std::path::Path;

/// Creates the backup or report base directory if it does not already exist
fn create_archive_dir(archdir: &Path, dry_run: bool) -> Result<String, String> {

    if archdir.is_dir() {
        info!("Found archive dir at {}", archdir.display());
    } else {
        info!("Can't find archive dir at {} so try to create it and check again!", archdir.display());
        if !archdir.exists() {
            // fs::create_dir_all(&archdir);
            if dry_run {
                println!("[dry-run]: Creating archive directory {}", archdir.display());
            } else {
                match fs::create_dir_all(&archdir) {
                    Ok(m) => { m }
                    Err(f) => { panic!("{}", f.to_string()) }
                };
            };
        };

        if !archdir.is_dir() {
            if !dry_run {
                // panic!("error: Cannot create {}", archdir.display());
                return Err(format!("Cannot create {}", archdir.display()));
            }
        }
    }

    Ok(format!("{} exists", archdir.display()))
}

/// Recursively copies a simple directory tree
/// Taken from <https://stackoverflow.com/a/65192210/13366>
fn copy_dir_all(src: impl AsRef<Path>, dst: impl AsRef<Path>) -> io::Result<()> {
    fs::create_dir_all(&dst)?;
    for entry in fs::read_dir(src)? {
        let entry = entry?;
        let ty = entry.file_type()?;
        if ty.is_dir() {
            copy_dir_all(entry.path(), dst.as_ref().join(entry.file_name()))?;
        } else {
            fs::copy(entry.path(), dst.as_ref().join(entry.file_name()))?;
        }
    }
    Ok(())
}

/// Enum for representing status conditions returned by RuntimeConfig objects
pub enum RuntimeConfigResult {
    // Errors
    BadOption,
    // Status
    Success,
    WantsHelp,
    WantsVersion
}

/// Properties of the current invocation of the program
struct RuntimeConfig {
    _rawargs: Vec<String>,
    _options: Options,
    program: String,
    arglist: Vec<String>,
    paramlist: HashSet<String>,
    want_report: bool,
    want_backup: bool,
    backup_all: bool,
    list_only: bool,
    force_overwrite: bool,
    dry_run: bool,
    logfilter: log::LevelFilter,
}

impl RuntimeConfig {
    /// Constructor
    pub fn new() -> RuntimeConfig {
        RuntimeConfig {
            _rawargs: env::args().collect(),
            _options: Options::new(),
            program: "__UNINITIALIZED".to_string(),
            arglist: vec!(),
            paramlist: HashSet::new(),
            want_report: false,
            want_backup: true,
            backup_all: false,
            list_only: false,
            force_overwrite: false,
            dry_run: false,
            logfilter: log::LevelFilter::Warn,
        }
    }

    /// Parse and perform light processing of command-line options
    pub fn consider_options(&mut self) -> Result<RuntimeConfigResult, RuntimeConfigResult> {
        self._rawargs = env::args().collect();

        self._options = Options::new();
        self._options.optflag("a", "all",     "Back up all save files");
        self._options.optflag("F", "force",   "Overwrite existing character backup");
        self._options.optflag("h", "help",    "Print this help menu");
        self._options.optflag("l", "list",    "List current saved games");
        self._options.optflag("n", "dry-run", "Find but do not copy files");
        self._options.optflag("r", "report",  "Collate for reporting an issue");
        self._options.optflag("v", "verbose", "Show more detailed log info");
        self._options.optflag("V", "version", "Print version info and exit");

        // Example option with value
        // opts.optopt("r", "report", "set output file name", "NAME");

        self.program = self._rawargs[0].clone();
        self.arglist = self._rawargs[1..].to_vec();

        // Match args with options or die gracefully
        let matches = match self._options.parse(&self.arglist) {
            Ok(m) => { m }
            // Err(..) => {
            Err(..) => {
                self.print_usage();
                println!("\nError: Unrecognized option(s)");
                println!("Please review the argument list {:?} to {}\n", &self.arglist, &self.program);
                return Err(RuntimeConfigResult::BadOption)
            }
            // Err(f) => { panic!("{}", f.to_string()) }
        };

        // Normalize positional paramters into self.paramlist
        if !matches.free.is_empty() {
            self.paramlist = HashSet::from_iter(matches.free.clone());
        }

        // Implement -h, --help
        if matches.opt_present("h") {
            self.print_version();
            self.print_usage();
            // return;
            return Ok(RuntimeConfigResult::WantsHelp)
        };

        // Implement -V, --version
        if matches.opt_present("V") {
            self.print_version();
            // return;
            return Ok(RuntimeConfigResult::WantsVersion)
        };

        // Implement -n, --dry-run
        self.dry_run = matches.opt_present("n");

        // Implement -v, --verbose
        self.logfilter = if matches.opt_present("v") {
            // --verbose sets log level to debug
            log::LevelFilter::Debug
        } else {
            // Set default log level to warn
            log::LevelFilter::Warn
        };

        if matches.opt_present("l") {
            self.list_only = true;
            self.want_report = false;
            self.want_backup = false;
            self.force_overwrite = false;
            self.backup_all = false;
        } else {
            // Implement --report
            self.want_report = matches.opt_present("r");
            // backup and report are mutually exclusive
            self.want_backup = !self.want_report;

            // Note: --force and --all are only relevant for backup
            // Implement --force
            // Implement --all
            if self.want_backup {
                // backup
                self.force_overwrite = matches.opt_present("F");
                self.backup_all = matches.opt_present("a");
            } else {
                // report
                self.force_overwrite = false;
                self.backup_all = false;
            }
        }

        return Ok(RuntimeConfigResult::Success)
    }

    /// Prints identifying version of the code
    pub fn print_version(&self) {
        println!("ostrabak v0.1.1.2: Savegame archiver for Ostranauts");
    }

    /// Prints code usage information
    pub fn print_usage(&self) {
        let brief = format!("Usage: {} [options] SAVEID [ SAVEID ... ]", self.program);
        print!("{}", self._options.usage(&brief));
    }

}

/// Enum for representing status conditions returned by PlayerSave objects
pub enum PlayerSaveResult {
    // Errors
    DestinationExists,
    CannotCreateReportDirectory,
    CannotFindSaveDirectory,
    CannotCopyRequiredFileToReport,
    DirectoryCopyFailed,
    // Status
    DirectoryCopied,
    DryRun,
    ReportGenerated,
}

/// Savegame properties
pub struct PlayerSave {
    id: String,
    folder_name: String,
    save_path: String,
    player_name: String,
    last_save_timstamp: String,
    main_save: String,
    json_save: Vec<String>,
}

impl PlayerSave {
    /// Create new PlayerSave object based on savegame Path
    pub fn new_from_path (save_path: &Path) -> PlayerSave {

        lazy_static! {
            static ref SAVE_RE : Regex = Regex::new(r"^Save(\d+)$").expect("Failed to compile Save# regex");
        };

        // TBD: Parse out PlayerSave fields from save_path
        let base_name : &str = save_path.file_name().expect("Cannot extract filename from path")
                                          .to_str().expect("Can't stringify folder name");

        let save_id : String = match SAVE_RE.captures(&base_name) {
            Some(m) => { m[1].to_string() }
            None => { "0".to_string() }
        };

        PlayerSave {
            id: save_id,
            folder_name: base_name.to_string(),
            // folder_name: save_path.file_name().expect("Cannot extract filename from path").to_str().expect("Can't stringify folder name").to_string(),
            save_path: save_path.to_str().expect("Can't stringify path").to_string(),
            player_name: "".to_string(),
            last_save_timstamp: "".to_string(),
            main_save: "".to_string(),
            json_save: vec!(),
        }

    }

    /// Read savegame metadata from saveInfo.json
    pub fn read_saveinfo(&mut self) -> Option<&str> {
        let info_path = Path::new(self.save_path.as_str()).join("saveInfo.json");
        if info_path.is_file() {
            info!("Reading {}", info_path.display());
            // TODO @apthorpe Add lines 455-467 to parse fields from saveInfo.json
            let rawgsmetadata = fs::read_to_string(info_path).expect("Unable to read file");
            debug!("gsmetadata is {}", rawgsmetadata);
            let gsmetadata = match json::parse(&rawgsmetadata) {
                Ok(md) => { md }
                Err(f) => panic!("{}", f.to_string())
            };

            debug!("Prettified savegame JSON metadata {}", gsmetadata.pretty(2));

            if gsmetadata[0]["playerName"].is_string() {
                self.player_name = gsmetadata[0]["playerName"].to_string();
                info!("Player name is {}", self.player_name);
                let my_main_save : String = format!("{}.json", self.player_name);
                // Store self.main_save if file exists
                if Path::new(self.save_path.as_str()).join(&my_main_save).is_file() {
                    self.main_save = my_main_save;
                    info!("Found main save file at {}", self.main_save);
                }
                // Populate json_save with existing old save files
                let pattern_oldsave = format!(r"^{}(_\d+)?\.bak$", self.player_name);
                let re_oldsave : Regex = Regex::new(pattern_oldsave.as_str())
                    .expect("Failed to compile old save regex");
                debug!("Checking for old save files with regex {}", pattern_oldsave);
                // self.json_save.push(each_old_save_file);

                let save_entries = fs::read_dir(&self.save_path)
                    .expect("Player save directory exists but is unreadable");

                for my_entry in save_entries {
                    // Releases path() of direntry or panic!s
                    let entry_path = my_entry.expect("Cannot extract path from strange directory entry").path();
                    // println!("Name: {}", entry_path.display());

                    let oldsave_name = entry_path.file_name().expect("Cannot extract file name")
                        .to_str().expect("Cannot stringify last path component");

                    if entry_path.is_file() {
                        if re_oldsave.is_match(oldsave_name) {
                            info!("Old save: {}", oldsave_name);
                            self.json_save.push(oldsave_name.to_string());
                        }
                    }
                }
            }

            if !gsmetadata[0]["strName"].is_null() {
                self.id = gsmetadata[0]["strName"].to_string();
                info!("Save id is {}", self.id);
            }

            if !gsmetadata[0]["realWorldTime"].is_null() {
                self.last_save_timstamp = gsmetadata[0]["realWorldTime"].to_string();
                info!("Save time is {}", self.last_save_timstamp);
            }

            Some(self.player_name.as_str())
        } else {
            None
        }
    }

    /// Copy savegames to backup directory
    pub fn write_backup(&self, bakdir : &Path, force : bool, dry_run : bool) -> Result<PlayerSaveResult, PlayerSaveResult> {
        let destfolder = format!("{}_{}", &self.folder_name, &self.player_name);
        let destdir = bakdir.join(destfolder);
        println!("Request slot {} be backed up to {}", &self.id, destdir.display());

        if destdir.exists() && !force {
            warn!("Cannot copy {} to {}; destination directory already exists and --force not specified", &self.folder_name, destdir.display());
            return Err(PlayerSaveResult::DestinationExists);
        } else {
            if dry_run {
                println!("[dry-run] Backing up {} ({}) to {}", &self.folder_name, &self.player_name, destdir.display());
                return Ok(PlayerSaveResult::DryRun);
            } else {
                let copy_status = match copy_dir_all(&self.save_path, &destdir) {
                    Ok(..) => { Ok(PlayerSaveResult::DirectoryCopied) }
                    Err(..) => { Err(PlayerSaveResult::DirectoryCopyFailed) }
                };
                // println!("Backed up {} ({}) to {}", &self.folder_name, &self.player_name, destdir.display());
                return copy_status;
            }
        }
    }

    /// Collate savegames and log info to report directory
    pub fn write_report(&self, rptdir : &Path, outlog : &Path, dry_run : bool) -> Result<PlayerSaveResult, PlayerSaveResult> {
        let digistamp: String = self.last_save_timstamp.chars().filter(|c| c.is_digit(10)).collect();
        let destfolder = format!("{}_{}_{}", &self.folder_name, &self.player_name, &digistamp);
        let destdir = rptdir.join(destfolder);
        println!("Request report for slot {} generated in {}", &self.id, destdir.display());

        if destdir.exists() {
            warn!("Cannot create report for {} in {}; report directory already exists", &self.folder_name, destdir.display());
            return Err(PlayerSaveResult::DestinationExists);
        } else {
            // Create destdir
            match create_archive_dir(&destdir, dry_run) {
                Ok(..) => { () }
                Err(..) => { return Err(PlayerSaveResult::CannotCreateReportDirectory) }
            };

            if dry_run {
                println!("[dry-run] Creating report for slot {} ({}) in {}", &self.folder_name, &self.player_name, destdir.display());
                return Ok(PlayerSaveResult::DryRun);
            } else {

                let srcdir = Path::new(&self.save_path);

                // Copy required files (result in hard failure)

                let mut req_files : Vec<&str> = vec!["saveInfo.json", "portrait.png"];
                let mut mspath = srcdir.join(&self.main_save);
                if mspath.is_file() {
                    // 0.7.0.3 and earlier
                    req_files.push(&self.main_save);
                } else {
                    // 0.7.0.4 and later
                    mspath = srcdir.join(&self.player_name);
                    if mspath.is_dir() {
                        let dstfn = destdir.join(&self.player_name);
                        match copy_dir_all(&mspath, &dstfn) {
                            Ok(..) => { () }
                            Err(..) => {
                                error!("Can't copy main save directory {} to report dir", mspath.display());
                                return Err(PlayerSaveResult::DirectoryCopyFailed);
                            }
                        };
                    }
                }

                for fname in req_files {
                    let srcfn = srcdir.join(&fname);
                    let dstfn = destdir.join(&fname);
                    match fs::copy(srcfn, dstfn) {
                        Ok(..) => { () }
                        Err(..) => {
                            error!("Can't copy {} to report dir", fname);
                            return Err(PlayerSaveResult::CannotCopyRequiredFileToReport);
                        }
                    }
                }

                // Copy optional files (result in soft failure)

                // Copy errlog to destdir

                if outlog.is_file() {
                    debug!("Found Ostranauts log file at {}!", outlog.display());
                    let destfile = outlog.file_name().expect("Can't isolate log file name");
                    fs::copy(outlog, destdir.join(&destfile)).expect("Can't copy log file");
                } else {
                    warn!("Cannot find Ostranauts log file at {}!", outlog.display());
                };

                return Ok(PlayerSaveResult::ReportGenerated);
            }
        }
    }
}

/// Create vector of PlayerSave objects based on savegames found in the filesystem
pub fn find_saves(basedir: &Path) -> Result<Vec<PlayerSave>, String> {
    let mut savelist : Vec<PlayerSave> = Vec::new();
    let save_re = Regex::new(r"^Save\d+$").expect("Failed to compile Save# regex");

    debug!("Finding saves under {}", basedir.display());

    if basedir.is_dir() {
        let save_entries = fs::read_dir(basedir).expect("Base save directory exists but is unreadable");

        for my_entry in save_entries {
            // Releases path() of direntry or panic!s
            let entry_path = my_entry.expect("Cannot extract path from strange directory entry").path();

            let folder_name = entry_path.file_name().expect("Cannot extract file name")
                .to_str().expect("Cannot stringify last path component");

            info!("Base savedir entry name: {}", folder_name);

            if entry_path.is_dir() {
                if save_re.is_match(folder_name) {
                    debug!("Save directory: {}", folder_name);
                    debug!("Create a new PlayerSave object and push it onto savelist.");

                    let mut ps = PlayerSave::new_from_path(&entry_path);
                    match ps.read_saveinfo() {
                        Some(m) => { info!("Found metadata for {}", m); }
                        None => { info!("No savegame metadata found"); }
                    };
                    savelist.push(ps);

                } else {
                    debug!("# Other directory: {}", folder_name);
                }
            } else {
                debug!("# Not a directory: {}", folder_name);
            }
        }
    } else {
        return Err("Cannot find save directory".to_string());
    }

    Ok( savelist )
}

/// Ostrabak main program for archiving saved games for Ostranauts
fn main() {

    let mut rcfg = RuntimeConfig::new();

    // Configure code from command-line options
    match &rcfg.consider_options() {
        Ok(RuntimeConfigResult::WantsHelp) => { return; }
        Ok(RuntimeConfigResult::WantsVersion) => { return; }
        Ok(..) => { () }
        Err(RuntimeConfigResult::BadOption) => { return; }
        Err(..) => {
            rcfg.print_usage();
            println!("\nError: Unrecognized option(s)");
            println!("Please review the argument list {:?}\n", &rcfg.arglist);
            return;
        }
    }

    // Initialize logging facility
    match SimpleLogger::new()
        .with_level(rcfg.logfilter)
        .init() {
        Ok(m) => { m }
        Err(f) => { panic!("{}", f.to_string()) }
    };

    debug!("Raw args: {:?}", rcfg._rawargs);

    // Detect home directory
    let userhome = match dirs::home_dir() {
        Some(m) => { m }
        None => { panic!("error: Cannot find home directory") }
    };

    info!("Your home directory is probably {}", userhome.display());

    // Detect path to Ostranauts saveganes
    let savedir = &userhome
        .join("AppData")
        .join("LocalLow")
        .join("Blue Bottle Games")
        .join("Ostranauts");

    let my_saves : Vec<PlayerSave> = match find_saves(savedir) {
        Ok(m) => { m }
        Err(f) => { panic!("{}", f.to_string()) }
    };

    // info!("Found {} save directories", my_saves.len());
    println!("Found {} save directories under {}", my_saves.len(), savedir.display());

    // Implement --list
    if rcfg.list_only {
        for pso in my_saves.iter() {
            println!("Slot {} ({}) contains savedata for {} dated {}",
                pso.id, pso.folder_name, pso.player_name, pso.last_save_timstamp);
        }
        return;
    };

    // Implement --all
    if rcfg.backup_all {
        info!("--all found; setting list of savedirs from valid_saveid");
    } else {
        info!("--all not found");

        // Show positional parameters (info)
        if rcfg.paramlist.is_empty() {
            error!("No positional parameters found");
            rcfg.print_usage();
            return;
        } else {
            info!("Found {} parameter(s)", rcfg.paramlist.len() );
            info!("Params: {:?}", rcfg.paramlist);
        };
    };

    if rcfg.want_backup {
        // Backup (--report not present)
        info!("--report not found, setting backup mode");

        // Create savedir/_backups if it doesn't already exist
        let bakdir = &savedir.join("_backups");

        // TODO @apthorpe refactor to routine and generalize for backup and report dirs
        match create_archive_dir(bakdir, rcfg.dry_run) {
            Ok(m) => { info!("{}", m) }
            Err(f) => { panic!("{}", f) }
        }

        // Iterate player save object (pso) over my_saves
        for pso in my_saves.iter() {
            println!("Slot {} ({}) contains savedata for {} dated {}",
                pso.id, pso.folder_name, pso.player_name, pso.last_save_timstamp);

            if rcfg.backup_all || rcfg.paramlist.contains(&pso.id) {

                match &pso.write_backup(&bakdir, rcfg.force_overwrite, rcfg.dry_run) {
                    Ok(PlayerSaveResult::DryRun) => { info!("Backup succeeded (dry-run)") }
                    Ok(..) => { info!("Backup succeeded") }
                    Err(PlayerSaveResult::DestinationExists) => {
                        warn!("Cannot back up {} to {}; destination alreadt exists but --force not specified",
                            &pso.folder_name, &bakdir.display())
                    }
                    Err(..) => { panic!("Failed to back up {} to {}", &pso.folder_name, &bakdir.display()) }
                };
            }
        }
    };

    // Implement --report
    if rcfg.want_report {
        info!("--report found, setting report mode");

        // Create savedir/_reports if it doesn't already exist
        let rptdir = &savedir.join("_reports");

        match create_archive_dir(rptdir, rcfg.dry_run) {
            Ok(m) => { info!("{}", m) }
            Err(f) => { panic!("{}", f) }
        }

        // Q: How do you get Windows drive letter?
        // A: kernel32::GetLogicalDrives(); see https://stackoverflow.com/a/33656994/13366
        // let outlogfile = &userhome.join("..").join("..")
        //     .join(r"Program Files (x86)")
        //     .join("Steam")
        //     .join("SteamApps")
        //     .join("common")
        //     .join("Ostranauts")
        //     .join("Ostranauts_Data")
        //     .join("output_log.txt");

        // TODO: @apthorpe Dynamically find or set proper drive letter. Brittle hardcoding
        // NOTE: outlogfile will contain a Path object created from outlogname
        // whether or not it can be canonicalized. outlogfile existence is checked
        // in PlayerSave::write_report(); failure to canonicalize here should not be fatal
        let outlogname = r"\Program Files (x86)\Steam\SteamApps\common\Ostranauts\Ostranauts_Data\output_log.txt";
        let outlogfile = match fs::canonicalize(outlogname) {
            Ok(m) => { info!("The Ostranauts output log is at {}", m.display()); m }
            Err(f) => { warn!("{}", f.to_string()); Path::new(outlogname).to_path_buf() }
        };

        // // info!("The Ostranauts output log is at {}", outlogfile.display());
        // if outlogfile.is_file() {
        //     info!("Ok, {} exists!", outlogfile.display());
        // };

        // Iterate saveentry over my_saves
        for pso in my_saves.iter() {
            println!("Slot {} ({}) contains savedata for {} dated {}",
                pso.id, pso.folder_name, pso.player_name, pso.last_save_timstamp);

            if rcfg.backup_all || rcfg.paramlist.contains(&pso.id) {

                match &pso.write_report(&rptdir, &outlogfile, rcfg.dry_run) {
                    Ok(PlayerSaveResult::DryRun) => { info!("Report succeeded (dry-run)") }
                    Ok(..) => { info!("Report succeeded") }
                    Err(PlayerSaveResult::DestinationExists) => {
                        warn!("Cannot create report for {} at {}; report already exists",
                            &pso.folder_name, &rptdir.display())
                    }
                    Err(..) => { panic!("Failed to create report for {} at {}", &pso.folder_name, &rptdir.display()) }
                };
            }
        }
    };

    // (If not --report):
    // Foreach unique positional argument (saveid):
    //   Warn and cycle if saveid not in [valid saveid list] Q: Or do earlier?
    //   Construct savegame path (savedir/Save# where # is saveid)
    //   Warn and cycle if savegame path is not found (do not warn if --all is set)
    //   Warn and cycle if savedir/Save#/saveInfo.json not found
    //   Warn and cycle if playerName value cannot be extracted from savedir/Save#/saveInfo.json
    //   Extract playerName value as string (Q: mutable?)
    //   Construct backup path of current savegame -> "savedir/_backups/Save# <playerName>"
    //     Q: Replace spaces in backup path with underscores?
    //   If backup path does not exist:
    //     Recursively copy "savedir/Save#"" to "savedir/_backups/Save# <playerName>"
    //   Else:
    //     If --force is set:
    //       Delete existing "savedir/_backups/Save# <playerName>"
    //       Recursively copy "savedir/Save#"" to "savedir/_backups/Save# <playerName>"
    //     Else:
    //       Warn of backup collision and cycle
    //     Endif
    //   Endif
    // End foreach

    // TBD1: If --report:
    // Foreach unique positional argument (saveid):
    //   Note: Steps marked with * are identical to those in the backup process above
    //   * Warn and cycle if saveid not in [valid saveid list] Q: Or do earlier?
    //   * Construct savegame path (savedir/Save# where # is saveid)
    //   * Warn and cycle if savegame path is not found (do not warn if --all is set)
    //   * Warn and cycle if savedir/Save#/saveInfo.json not found
    //   * Warn and cycle if playerName value cannot be extracted from savedir/Save#/saveInfo.json
    //   * Extract playerName value as string (Q: mutable?)
    //   Construct report path of current savegame -> "savedir/_reports/YYYMMMDDHHMMSS Save# <playerName>"
    //     Q: Replace spaces in backup path with underscores?
    //   While report path exists:
    //     Warn and sleep(2)
    //     Warn and cycle foreach if 5 attempts fail (failsafe)
    //     Regenerate report path with new timestamp
    //   End while
    //
    //   Foreach file in [saveInfo.json, <playerName>.json, portrait.png]:
    //     Copy from "savedir/Save#" to "savedir/_reports/YYYMMMDDHHMMSS Save# <playerName>"
    //   End foreach
    //   Copy output_log.txt to "savedir/_reports/YYYMMMDDHHMMSS Save# <playerName>" or warn
    //   Archive and compress "savedir/_reports/YYYMMMDDHHMMSS Save# <playerName>" to "savedir/_reports/YYYMMMDDHHMMSS Save# <playerName>.zip"
    //   Q: Can files be archived directly to zipfile without creating an uncompressed directory?
    // End foreach

    // Print summary of directories created (path)

}
