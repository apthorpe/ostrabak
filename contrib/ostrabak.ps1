$OstraSaveDir = $HOME + "\AppData\LocalLow\Blue Bottle Games\Ostranauts"

if (Test-Path $OstraSaveDir)
{
    cd $OstraSavedir
}
else
{
    write-host "error: Cannot find Ostranauts savegame folder at $OstraSaveDir"
    exit
}

$BakFolderName = "_backups"
$OstraBakdir = $OstraSavedir + "\" + $BakFolderName

if (!(Test-Path $OstraBakdir))
{
    New-Item -itemType Directory -Path $OstraSaveDir -Name $BakFolderName
}

if (Test-Path $OstraBakdir)
{
    write-host "info: Savegame backup folder set to $OstraBakDir"
}
else
{
    write-host "error: Cannot find or create savegame backup folder at $OstraBakDir"
    exit
}

