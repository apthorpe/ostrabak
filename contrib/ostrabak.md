# PDL for Ostranauts save game backups

Set cwd to user save game directory

Example: `C:\Users\apthorpe\AppData\LocalLow\Blue Bottle Games\Ostranauts`

```
$OstraSaveDir = $HOME + "\AppData\LocalLow\Blue Bottle Games\Ostranauts"

if (Test-Path $OstraSaveDir)
{
    cd $OstraSavedir
}
else
{
    write-host "Cannot find Ostranauts savegame folder at $OstraSaveDir"
    exit
}
```

Create `_backups` under `$OstraSaveDir` if it does not already exist.
Save the backup directory name as `$OstraBakdir`.

```
$BakFolderName = "_backups"
$OstraBakdir = $OstraSavedir + "\" + $BakFolderName

if (!(Test-Path $OstraBakdir))
{
    New-Item -itemType Directory -Path $OstraSaveDir -Name $BakFolderName
}

if (!(Test-Path $OstraBakdir))
{
    write-host "Cannot find or create savegame backup folder at $OstraBakDir"
    exit
}
```

## Notes

* Find proper idiom for terminating script on error conditions
* Use `Write-Error` instead of `write-host` for error messages
