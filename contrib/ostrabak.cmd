@REM %1 is the save ID number (1, 2, 3, *, or all). Default is 'ALL'
@REM %2 is the option REPORT. This is valid only if %1 is set to 1, 2, or 3 otherwise is considered an error
@echo off
setlocal enableextensions

set saveid=%~1
if '%saveid%'=='' set saveid=all
if '%saveid%'=='*' set saveid=all

set rptflag=FALSE
if not '%saveid%'=='all' (
	if '%~2'=='report' set rptflag=TRUE
)

echo saveid is %saveid%
echo rptflag is %rptflag%

set OSAVDIR=%USERPROFILE%\AppData\LocalLow\Blue Bottle Games\Ostranauts

if exist "%OSAVDIR%" (
    cd "%OSAVDIR%"
) else (
    echo error: Cannot find Ostranauts savegame folder at "%OSAVDIR%"
    exit /b 1
)

set BAKDN=_backups
set OBAKDIR=%OSAVDIR%\%BAKDN%

if not exist "%OBAKDIR%" (
    echo info: Creating backup directory "%OBAKDIR%"
    mkdir "%OBAKDIR%"
)

if exist "%OBAKDIR%" (
    echo info: Savegame backup folder set to "%OBAKDIR%"
) else (
    echo error: Cannot find or create savegame backup folder at "%OBAKDIR%"
    exit /b 2
)

@REM forfiles /P %OSAVDIR% /M Save* /C "cmd /C echo Found @file"
for /d %%d in (Save1 Save2 Save3) do (
	if exist %%d (
		echo Found %%d
		if exist %%d\saveInfo.json (
			echo Found saveInfo.json
			echo Raw player name is:
@REM 			findstr /c:playerName %%d\saveInfo.json

			for /f "tokens=* usebackq" %%f in (`findstr /c:playerName %%d\saveInfo.json`) do (
				echo Matchline: %%f
				for /f "tokens=* delims=:" %%t in ("%%f") do (
					echo Token: %%t
				)
			)

			echo Primary save file is [one of]
			dir /b %%d\*.json | findstr /v /c:saveInfo
			echo.
		)
	) else (
		echo Cannot find %%d
	)

)

exit /b 0
